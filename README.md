# GeneOntologySPARQL

This project aims at exploring the [Gene Ontology](http://geneontology.org/) using SPARQL.
## 1. Access data

You can either

- Use the Gene Ontology public SPARQL endpoint [http://geneontology.org/sparql.html](http://geneontology.org/sparql.html). 
Be aware that they seem to have enabled automatic inference (or generated the transitive closure of `rdfs:subClassOf` which can be either a good thing because you don't have to do it=, or a bad thing if you want to retrieve the direct superclass or subclass of a GO term).
    - pros:
        - you do not have to set up an endpoint
        - always up to date
    - cons:
        - you do not learn to set up an endpoint
        - requires an internet connection
- Or set up your own endpoint with your local copy of GO
    - pros:
        - can work offline
        - facilitates the exploration of the dataset (somehow)
        - you get to learn to set up your own endpoint
    - cons:
        - requires some tinkering
    - do not even try to download uniprot :-)
    - retrieve the `go.owl` file from [http://geneontology.org/docs/download-ontology/](http://geneontology.org/docs/download-ontology/) (permanent URL: [http://purl.obolibrary.org/obo/go.owl](http://purl.obolibrary.org/obo/go.owl))
    - with [Apache fuseki]()
        - see [https://gitlab.com/odameron/rdf-sparql-cheatsheet](https://gitlab.com/odameron/rdf-sparql-cheatsheet)
        - download `apache-jena-fuseki-X.Y.Z.tar.gz` from [https://jena.apache.org/download/](https://jena.apache.org/download/)
        - in a terminal:
            - (optional) declare a `FUSEKI_HOME` variable: `export FUSEKI_HOME=/path/to/apache-jena-fuseki-X.Y.Z/`
            - start fuseki (no trailing `&` and leave the terminal open): `${FUSEKI_HOME}/fuseki-server --file=/path/to/data/go.owl /go`
        - in a web browser, open [http://localhost:3030](http://localhost:3030)


## 2. Explore the surroundings of a GO term

The goal of this section is to understand the local data structure at the level of a GO term.

Your queries will elaborate on the following template:

```sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

PREFIX go: <http://purl.obolibrary.org/obo/GO_>
PREFIX oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>

SELECT *
WHERE {
  VALUES ?go { go:0000018 }
  # ?go ...
}
```

- display all the triples having `go:0000018` as a subject
- figure out the URI of the relations of interest:
    - its type
    - its superclasses
    - its ID (the local part of the URI, here as a string)
    - its label
- write the SPARQL query that retrieves the `rdfs:label` of a GO term. For example, for `go:0000018` it should be `"regulation of DNA recombination"`
- write the SPARQL query that retrieves the URI of the GO term that has a specific label. For example, the GO term that has `"regulation of DNA recombination"` as a label is `go:0000018`


## 3. Explore the hierarchy of a GO term

The goal of this section is to explore the taxonomical hierarchy of a GO term.

![Hierarchy of go:0000018](figures/goHierarchy-regulationOfDNArecombination.png)

- write a query to retrieve the URI of the direct superclasses of a GO term. For `go:0000018` there should be two: one of them is another GO term (`go:0051052`), and the other one is a blank node (we will come back to this later)
- adapt the query so that it only retrieves the direct superclasses of a GO term that are also instances of `owl:Class`.
- write a query to retrieve the URI of the ancestors of a GO term (i.e. its direct or indirect superclasses) that are also instances of `owl:Class`. For `go:0000018` there should be 11 ancestors.
- write a query to retrieve the URI of the direct subclasses of a GO term that are also instances of `owl:Class`. For `go:0000018` there should be 10 direct subclasses.
- write a query to retrieve the URI of the descendant of a GO term (i.e. its direct or indirect subclasses) that are also instances of `owl:Class`. For `go:0000018` there should be 50 descendants.
- write a query to retrieve all the class-superclass relations between GO terms that are ancestors of a GO term
- write a query to retrieve all the subclass-class relations between GO terms that are descendants of a GO term



## 4. Explore the branches of GO

The goal of this section is to scale from individual GO terms to GO branches.

For each of `go:0008150` (biological process), `go:0005575` (cellular component) and `go:0003674` (molecular function)

- write a query to compute their number of descendant GO terms
- write a query to compute the number of `rdfs:subClassOf` edges


## 5. Consider relations other than `rdfs:subClassOf` by taking `owl:Restriction` into account

- (bonus) adapt the previous queries to also take into account the other relations through `owl:Restrictions` (cf schema below). Check with the parents of `go:0000018` first
- (bonus) are there any GO term that belongs to two of the branches? (considering only `rdfs:subClassOf` or also the other relations)
- (bonus) retrieve the common ancestors of 2 GO terms, and then the lowest common ancestor(s) of these 2 GO termsS

![Representation of relations other than `rdfs:subClassOf` in the Gene Ontology hierarchy](figures/go-existentialRestrictionExample.png)


## 6. (Lowest) common ancestor(s)

- write a query that retrieves the GO terms that are ancestors of two GO terms (e.g. of `go:0019219` and of `go:0006259`)
- write a query that retrieves the lowest common of these ancestors (which may not be unique) by determining the common ancestor(s) that do not have any other common ancestors as descendants.
